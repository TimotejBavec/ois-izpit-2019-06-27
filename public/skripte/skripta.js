/* global L, distance, $ */

// seznam z markerji na mapi
var markerji = [];

// Seznam krogov na mapi
var krogi = [];


var mapa;
var vprasanja = [];
var st_vprasanja = 0;
var st_poskusov = 0;
var st_vseh_poskusov = 0;


// Spremenljivke, kjer je shranjena izbira uporabnika na začetku igre
var st_vseh_vprasanj = 0;
var vzdevek = "";



/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 * @param popup skrij oblaček (privzeto) ali ga prikaži
 */
function dodajMarker(lat, lng, opis, popup) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  var marker = L.marker([lat, lng], {icon: ikona});
  marker.addTo(mapa);
  if (popup) {
    marker.bindPopup("<div>" + opis + "</div>").openPopup();
  } else {
    marker.bindPopup("<div>" + opis + "</div>");
  }
  markerji.push(marker);
}


/**
 * Dodaj krog na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param r polmer kroga v km
 * @param barva opcijsko ime barve (privzeta je modra)
 */
function dodajKrog(lat, lng, r, barva) {
  if (barva == undefined) {
    barva = "#3388ff";
  }
  var krog = L.circle([lat,lng], r * 1000, {color: barva, opacity:.5});
  krog.addTo(mapa);
  krogi.push(krog);
}


/**
 * Z zemljevida izbriši vse obstoječe oznake in kroge
 */
function izbrisiPrejsnjeOznakeInKroge() {
  for (var i = 0; i < markerji.length; i++)
    mapa.removeLayer(markerji[i]);
  for (var i = 0; i < krogi.length; i++)
    mapa.removeLayer(krogi[i]);
}


/**
 * Preverjanje ali sta 1. lokacija (lat1, lng1) s podanim radijem r1,
 * ki predstavlja lokacijo pravilnega odgovora
 * in 2. lokacija (lat2, lng2) s podanim radijem r2,
 * ki predstavlja uporabniško izbrano točko na zemljevidu, dovolj blizu.
 *
 * Če sta lokaciji dovolj blizu, je odgovor pravilen.
 *
 * @param lat1 zemljepisna širina pravilnega odgovora
 * @param lng1 zemljepisna dolžina pravilnega odgovora
 * @param r1 radij pravilnega odgovora
 * @param lat2 zemljepisna širina uporabniško izbrane točke na zemljevidu
 * @param lng2 zemljepisna dolžina uporabniško izbrane točke na zemljevidu
 * @param r2 radij uporabniško izbrane točke na zemljevidu
 */
function preveriOdgovor(lat1, lng1, r1, lat2, lng2, r2) {
  st_poskusov++;
  st_vseh_poskusov++;
  var d = distance(lat1, lng1, lat2, lng2, "K");
  var r = Math.max(r1, r2);

  izbrisiPrejsnjeOznakeInKroge();
  dodajMarker(lat2, lng2,
    d <= r ? ("Čestitam, odgovor <b>" +
      vprasanja[st_vprasanja - 1]["Pravilni odgovor"] + "</b> je pravilen!<br>" +
      "<button class='rob senca' onclick='prikaziNovoVprasanje()'>Klikni za nadaljevanje</button>") :
    ("<b>Izbrana lokacija</b><br>(" + lat2 + ", " + lng2 + ")"),
    d <= r);
  dodajKrog(lat2, lng2, r2);

  if (d <= r) {
    $("#povzetek").removeClass("skrito");
    $("#tabela_tock").append("<b>" + st_vprasanja + ". vprašanje" + "</b> :" +
      "<span style='padding-left:10px'>" + st_poskusov + " poskus" +
      (st_poskusov > 4 ? "ov" : (st_poskusov >= 3 ? "i" : (st_poskusov == 2 ? "a" : ""))) + "</span><br>");
    st_poskusov = 0;
  }

}


/**
 * Prikaži novo vprašanje tako, da povečaš zaporedno številko trenutnega
 * vprašanja, izbrišeš vse oznake in kroge na zemljevidu ter v primeru,
 * da igra še ni končana, prikažeš novo vprašanje, sicer se igra zaključi.
 */
function prikaziNovoVprasanje() {
  st_vprasanja++;
  izbrisiPrejsnjeOznakeInKroge();
  if (st_vprasanja > vprasanja.length) {
    var rezultat = Math.round(100 *  vprasanja.length / st_vseh_poskusov, 2);

    $("#odziv").html("<p>Igro ste končali z rezultatom <b>" + rezultat +
      "%</b>!</p>" + "<p><a class='povezava' href='/'>Poskusi znova</a>" +
      " ali poglej " + "<a class='povezava' href='/lestvica'>lestvico najboljših</a></p>");
    $("#zap_st_vprasanja").html("");
    $("#vsebina_vprasanja").html("");
    $("#navodila").addClass("skrito");
    $("#povezava-lestvica").addClass("skrito");

    // Shrani rezultat na lestvico najboljših igralcev
    $.get('/lestvica/' + JSON.stringify({
      cas: new Date(),
      vzdevek: "Neznalec",
      stVprasanj: 1,
      rezultat: 0
    }));
  } else {
    $("#odziv").html("");
    $("#zap_st_vprasanja").html(st_vprasanja +
      ". vprašanje (od skupno " + vprasanja.length + ")");
    $("#vsebina_vprasanja").html(vprasanja[st_vprasanja - 1]["Vprašanje"]);
  }
}



/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti zemljevida
  var mapOptions = {
    center: [46.119944, 14.815333],
    zoom: 9,
    minZoom: 8,
    maxZoom: 11
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  $("#gumbZacni").click(function() {
    st_vseh_vprasanj = parseInt($("#steviloVprasanj").val(), 10);
    vzdevek = $("#vzdevek").val();
    if ($("#vzdevek").val==null){
      vzdevek = "Anonimnež"
    }
  });

  function obKlikuNaMapo(e) {
    var koordinate = e.latlng;

    // Če ni nobenega vprašanja, ne naredi ničesar
    if ($("#navodila").hasClass("skrito"))
      return;

    preveriOdgovor(vprasanja[st_vprasanja - 1]["Pravilne koordinate"]["lat"],
      vprasanja[st_vprasanja - 1]["Pravilne koordinate"]["lng"], 5,
      koordinate.lat, koordinate.lng, 5);
  }

  mapa.on('click', obKlikuNaMapo);
});
